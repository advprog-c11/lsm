/*
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package id.advprog.c11.lsm;

import static id.advprog.c11.lsm.servismotor.ServisMotorFactory.createServis;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import id.advprog.c11.lsm.motor.Motor;
import id.advprog.c11.lsm.repository.ServisMotorRepository;
import id.advprog.c11.lsm.servismotor.ServisMotor;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JacksonJsonParser;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureTestEntityManager
public class LsmApplicationTests {

    @Autowired
    private MockMvc springInfrastructure;

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ServisMotorRepository servisMotorRepository;

    @Test
    public void pesanServisGivenNoTokenTest() throws Exception {
        String requestBody =
                "{\"tipe\": \"Bebek\", \"platNomor\": \"1234\", "
                        + "\"merek\": \"Honda\", \"warna\": \"Biru\"}";

        springInfrastructure.perform(
                MockMvcRequestBuilders.post("/pesan-servis")
                        .header("Origin", "https://lsm.netlify.com")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @Transactional
    public void pesanServisTest() throws Exception {

        String accessToken = obtainAccessToken("user", "user ");
        String requestBody =
                "{\"tipe\": \"Bebek\", \"platNomor\": \"1234\", "
                        + "\"merek\": \"Honda\", \"warna\": \"Biru\"}}";

        springInfrastructure.perform(
                MockMvcRequestBuilders.post("/pesan-servis")
                        .header("Origin", "https://lsm.netlify.com")
                        .param("access_token", accessToken)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.motor.platNomor").value("1234"))
                .andExpect(jsonPath("$.motor.merek").value("Honda"))
                .andExpect(jsonPath("$.motor.warna").value("Biru"))
                .andExpect(jsonPath("$.motor.description")
                        .value("Motor Bebek\nPlat Nomor: 1234\nMerek: Honda\n Warna: Biru"))
                .andExpect(jsonPath("$.username").value("user"));
    }

    private String obtainAccessToken(String username, String password) throws Exception {
        String requestBody =
                "username=" + username + "&password=" + password + "&grant_type=password";

        ResultActions result =
                springInfrastructure.perform(
                        MockMvcRequestBuilders.post("/oauth/token")
                                .header("Access-Control-Request-Method", "POST")
                                .header("Origin", "https://lsm.netlify.com")
                                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                                .content(requestBody)
                                .with(httpBasic("my-trusted-client", "secret"))
                                .accept("application/json;charset=UTF-8"))
                        .andExpect(status().isOk())
                        .andExpect(content().contentType("application/json;charset=UTF-8"));

        String resultString = result.andReturn().getResponse().getContentAsString();

        JacksonJsonParser jsonParser = new JacksonJsonParser();
        return jsonParser.parseMap(resultString).get("access_token").toString();
    }

    @Test
    @Transactional
    public void riwayatTest() throws Exception {
        List<ServisMotor> servis = new ArrayList<>();
        servis.add(createServis(new Motor("Bebek", "A 1234 BCD", "Hedon", "Ungu"),
                "Jaelani"));
        servis.add(createServis(new Motor("Kopling", "B 5678 EFG", "Mahalya", "Jingga"),
                "Jaelani"));
        servis.add(createServis(new Motor("Matic", "C 9012 HIJ", "Sawahkaki", "Polkadot"),
                "Jaelani"));
        servis.forEach(s -> s.setUsername("user"));
        servis.forEach(s -> entityManager.persist(s));
        entityManager.flush();

        String accessToken = obtainAccessToken("user", "user");

        springInfrastructure.perform(
                MockMvcRequestBuilders.get("/riwayat")
                        .header("Origin", "https://lsm.netlify.com")
                        .param("access_token", accessToken)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(4)));
    }

    @Test
    @Transactional
    public void registerTest() throws Exception {
        String requestBody =
                "{ \"username\": \"test\", \"password\":\"test\" }";

        springInfrastructure.perform(
                MockMvcRequestBuilders.post("/register")
                        .header("Origin", "https://lsm.netlify.com")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success").value(true));
    }
}
