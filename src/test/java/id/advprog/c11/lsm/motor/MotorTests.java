package id.advprog.c11.lsm.motor;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MotorTests {

    private Motor motor;

    @Before
    public void initiateMotor() {
        motor = new Motor("Bebek","B3333XZ", "Honda", "Biru");
    }

    @Test
    public void motorDescriptionTest() {
        assertEquals(motor.getDescription(),
                "Motor Bebek\nPlat Nomor: B3333XZ\nMerek: Honda\n Warna: Biru");
    }

}
