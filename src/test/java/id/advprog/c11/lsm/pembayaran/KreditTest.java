package id.advprog.c11.lsm.pembayaran;

import static id.advprog.c11.lsm.servismotor.ServisMotorFactory.createServis;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import id.advprog.c11.lsm.motor.Motor;
import id.advprog.c11.lsm.servismotor.ServisMotor;
import org.junit.Before;
import org.junit.Test;

public class KreditTest {
    private Pembayaran kredit;

    @Before
    public void setUp() {
        kredit = new Kredit("0123456789123456");
    }

    @Test
    public void testNomorKartuDebitIs16DigitsLong() {
        assertThrows(IllegalArgumentException.class, () -> new Kredit("12345"));
        assertThrows(IllegalArgumentException.class, () -> new Kredit("01234567891234567"));
    }

    @Test
    public void testNomorKartuDebitIsNumeric() {
        assertThrows(IllegalArgumentException.class, () -> new Kredit("abcdefghijklmnop"));
        assertThrows(IllegalArgumentException.class, () -> new Kredit("0123456789abcdef"));
    }

    @Test
    public void testBayar() {
        ServisMotor servisMotorBebek = createServis(
                new Motor("Bebek", "A 1234 BCD", "Hando", "Cokelat"),
                "Jaelani");
        kredit.setServisMotor(servisMotorBebek);
        String hasil = kredit.bayar();
        assertEquals(
            String.format(
                "Berhasil membayar sebesar Rp%.2f dengan kartu kredit yang berakhiran 3456.",
                servisMotorBebek.getHarga()
            ),
            hasil
        );
    }

}
