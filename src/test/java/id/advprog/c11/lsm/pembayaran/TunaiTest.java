package id.advprog.c11.lsm.pembayaran;

import static id.advprog.c11.lsm.servismotor.ServisMotorFactory.createServis;
import static org.junit.jupiter.api.Assertions.assertEquals;

import id.advprog.c11.lsm.motor.Motor;
import id.advprog.c11.lsm.servismotor.ServisMotor;
import org.junit.Before;
import org.junit.Test;

public class TunaiTest {
    private Pembayaran tunai;

    @Before
    public void setUp() {
        tunai = new Tunai();
    }

    @Test
    public void testBayar() {
        ServisMotor servisMotorBebek = createServis(
                new Motor("Bebek", "A 1234 BCD", "Hando", "Cokelat"),
                "Jaelani");
        tunai.setServisMotor(servisMotorBebek);
        String hasil = tunai.bayar();
        assertEquals(
            String.format(
                "Berhasil membayar sebesar Rp%.2f dengan uang tunai.",
                servisMotorBebek.getHarga()
            ),
            hasil
        );
    }

}
