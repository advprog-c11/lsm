package id.advprog.c11.lsm.pembayaran;

import static id.advprog.c11.lsm.servismotor.ServisMotorFactory.createServis;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import id.advprog.c11.lsm.motor.Motor;
import id.advprog.c11.lsm.servismotor.ServisMotor;

import java.lang.reflect.Modifier;
import org.junit.Test;

public class PembayaranTest {
    @Test
    public void testPembayaranIsAPublicAbstractClass() {
        int classModifiers = Pembayaran.class.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isAbstract(classModifiers));
    }

    @Test
    public void testPembayaranSudahDibayar() {
        ServisMotor servisMotorBebek = createServis(
                new Motor("Bebek", "A 1234 BCD", "Hando", "Cokelat"),
                "Jaelani");
        Pembayaran metode = new Pembayaran() {};
        metode.setServisMotor(servisMotorBebek);
        metode.bayar();
        String hasil = metode.bayar();
        assertEquals("Servis ini sudah dibayar!", hasil);
    }
}
