package id.advprog.c11.lsm.servismotor;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import id.advprog.c11.lsm.motor.Motor;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

public class ServisMotorTests {

    private ServisMotor servisMotor;

    @Test
    public void servisMaticTest() throws Exception {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        servisMotor = ServisMotorFactory.createServis(
                new Motor("Matic", "B1234Z", "Beat", "Merah"),
                "Jaelani");
        System.out.println(servisMotor.cekRem());
        System.out.println(servisMotor.cekAki());
        System.out.println(servisMotor.cekMesin());
        System.out.println(servisMotor.cekBan());
        String expectedOutput =
                "Rem motor dicek sesuai dengan cara pengecekan rem motor matic\n"
                        + "Aki motor dicek sesuai dengan cara pengecekan aki motor matic\n"
                        + "Mesin motor dicek sesuai dengan cara pengecekan mesin motor matic\n"
                        + "Ban motor dicek sesuai dengan cara pengecekan ban motor matic\n";

        assertEquals(expectedOutput.replaceAll("\n", "").replaceAll("\r", ""),
                outContent.toString().replaceAll("\n", "").replaceAll("\r", ""));
    }
    
    @Test
    public void servisKoplingTest() throws Exception {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        servisMotor = ServisMotorFactory.createServis(
                new Motor("Kopling", "B1234Z", "Jupiter", "Biru"),
                "Jaelani");
        System.out.println(servisMotor.cekRem());
        System.out.println(servisMotor.cekAki());
        System.out.println(servisMotor.cekMesin());
        System.out.println(servisMotor.cekBan());
        String expectedOutput =
                "Rem motor dicek sesuai dengan cara pengecekan rem motor kopling\n"
                        + "Aki motor dicek sesuai dengan cara pengecekan aki motor kopling\n"
                        + "Mesin motor dicek sesuai dengan cara pengecekan mesin motor kopling\n"
                        + "Ban motor dicek sesuai dengan cara pengecekan ban motor kopling\n";

        assertEquals(expectedOutput.replaceAll("\n", "").replaceAll("\r", ""),
                outContent.toString().replaceAll("\n", "").replaceAll("\r", ""));
    }

    @Test
    public void servisBebekTest() throws Exception {
        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        servisMotor = ServisMotorFactory.createServis(
                new Motor("Bebek", "B1234Z", "Jupiter", "Biru"),
                "Jaelani");
        System.out.println(servisMotor.cekRem());
        System.out.println(servisMotor.cekAki());
        System.out.println(servisMotor.cekMesin());
        System.out.println(servisMotor.cekBan());
        String expectedOutput =
                "Rem motor dicek sesuai dengan cara pengecekan rem motor bebek\n"
                        + "Aki motor dicek sesuai dengan cara pengecekan aki motor bebek\n"
                        + "Mesin motor dicek sesuai dengan cara pengecekan mesin motor bebek\n"
                        + "Ban motor dicek sesuai dengan cara pengecekan ban motor bebek\n";

        assertEquals(expectedOutput.replaceAll("\n", "").replaceAll("\r", ""),
                outContent.toString().replaceAll("\n", "").replaceAll("\r", ""));
    }

    @Test
    public void unsupportedServisTest() {
        assertThrows(
            IllegalArgumentException.class, () -> ServisMotorFactory.createServis(
                new Motor("Moge", "B9876A", "Harley Quinn", "Hitam"),
                        "Jaelani"
            )
        );
    }
}
