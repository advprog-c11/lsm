package id.advprog.c11.lsm.servismotor;

import id.advprog.c11.lsm.motor.Motor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class ServisMotorFactory {
    private static final String TIPE_MOTOR_TIDAK_TERSEDIA = "Tipe motor tidak tersedia!";

    public static ServisMotor createServis(Motor motor, String username) {
        return createServis(
                motor.getTipe(), motor.getPlatNomor(), motor.getMerek(), motor.getWarna(), username
        );
    }

    public static ServisMotor createServis(
            String tipeMotor, String platNomor, String merek, String warna, String username
    ) throws IllegalArgumentException {
        switch (tipeMotor) {
            case "Bebek":
                return new ServisMotorBebek(
                    new Motor("Bebek", platNomor, merek, warna), username
                );
            case "Kopling":
                return new ServisMotorKopling(
                    new Motor("Kopling", platNomor, merek, warna), username
                );
            case "Matic":
                return new ServisMotorMatic(
                    new Motor("Matic", platNomor, merek, warna), username
                );
            default:
                throw new IllegalArgumentException(TIPE_MOTOR_TIDAK_TERSEDIA);
        }
    }
}
