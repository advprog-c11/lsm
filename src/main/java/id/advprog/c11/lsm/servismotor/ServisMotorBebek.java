package id.advprog.c11.lsm.servismotor;

import id.advprog.c11.lsm.motor.Motor;
import javax.persistence.Entity;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
public class ServisMotorBebek extends ServisMotor {
    private static final double HARGA = 167000.00;

    public ServisMotorBebek(Motor motor, String username) {
        super(motor, username, 18000, 22000, 24000, 15000);
    }

    @Override
    public String cekRem() {
        return "Rem motor dicek sesuai dengan cara pengecekan rem motor bebek";
    }

    @Override
    public String cekAki() {
        return "Aki motor dicek sesuai dengan cara pengecekan aki motor bebek";
    }

    @Override
    public String cekMesin() {
        return "Mesin motor dicek sesuai dengan cara pengecekan mesin motor bebek";
    }

    @Override
    public String cekBan() {
        return "Ban motor dicek sesuai dengan cara pengecekan ban motor bebek";
    }

    @Override
    public String selesai() {
        return "Servis motor bebek sudah selesai";
    }

    @Override
    public double getHarga() {
        return HARGA;
    }
}
