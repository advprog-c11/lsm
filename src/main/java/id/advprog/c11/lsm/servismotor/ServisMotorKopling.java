package id.advprog.c11.lsm.servismotor;

import id.advprog.c11.lsm.motor.Motor;
import javax.persistence.Entity;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
public class ServisMotorKopling extends ServisMotor {
    private static final double HARGA = 200000.00;

    public ServisMotorKopling(Motor motor, String username) {
        super(motor, username, 23000, 17000, 22000, 18000);
    }

    @Override
    public String cekRem() {
        return "Rem motor dicek sesuai dengan cara pengecekan rem motor kopling";
    }

    @Override
    public String cekAki() {
        return "Aki motor dicek sesuai dengan cara pengecekan aki motor kopling";
    }

    @Override
    public String cekMesin() {
        return "Mesin motor dicek sesuai dengan cara pengecekan mesin motor kopling";
    }

    @Override
    public String cekBan() {
        return "Ban motor dicek sesuai dengan cara pengecekan ban motor kopling";
    }

    @Override
    public String selesai() {
        return "Servis motor kopling sudah selesai";
    }

    @Override
    public double getHarga() {
        return HARGA;
    }
}
