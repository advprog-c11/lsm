package id.advprog.c11.lsm.servismotor;

import id.advprog.c11.lsm.motor.Motor;
import javax.persistence.Entity;
import lombok.NoArgsConstructor;

@Entity
@NoArgsConstructor
public class ServisMotorMatic extends ServisMotor {
    private static final double HARGA = 189000.00;

    public ServisMotorMatic(Motor motor, String username) {
        super(motor, username, 18000, 20000, 19000, 18000);
    }

    @Override
    public String cekRem() {
        return "Rem motor dicek sesuai dengan cara pengecekan rem motor matic";
    }

    @Override
    public String cekAki() {
        return "Aki motor dicek sesuai dengan cara pengecekan aki motor matic";
    }

    @Override
    public String cekMesin() {
        return "Mesin motor dicek sesuai dengan cara pengecekan mesin motor matic";
    }

    @Override
    public String cekBan() {
        return "Ban motor dicek sesuai dengan cara pengecekan ban motor matic";
    }

    @Override
    public String selesai() {
        return "Servis motor matic sudah selesai";
    }

    @Override
    public double getHarga() {
        return HARGA;
    }
}
