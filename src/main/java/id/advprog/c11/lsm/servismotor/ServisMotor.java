package id.advprog.c11.lsm.servismotor;

import id.advprog.c11.lsm.motor.Motor;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@NoArgsConstructor
public abstract class ServisMotor {
    protected int status;
    protected @Embedded Motor motor;
    protected boolean sudahDibayar;
    protected String username;
    protected long cekRemDelay;
    protected long cekAkiDelay;
    protected long cekMesinDelay;
    protected long cekBanDelay;
    private @Id @GeneratedValue Long id;

    public ServisMotor(Motor motor, String username, long cekRemDelay,
                       long cekAkiDelay, long cekMesinDelay, long cekBanDelay) {
        this.motor = motor;
        this.username = username;
        this.cekRemDelay = cekRemDelay;
        this.cekAkiDelay = cekAkiDelay;
        this.cekMesinDelay = cekMesinDelay;
        this.cekBanDelay = cekBanDelay;
    }

    public void servis() {
        cekRem();
        cekAki();
        cekMesin();
        cekBan();
        selesai();
    }

    public abstract String cekRem();

    public abstract String cekAki();

    public abstract String cekMesin();

    public abstract String cekBan();

    public abstract String selesai();

    public abstract double getHarga();
}
