package id.advprog.c11.lsm;

import id.advprog.c11.lsm.authentication.entities.Role;
import id.advprog.c11.lsm.authentication.entities.User;
import id.advprog.c11.lsm.http.request.PembayaranRequestBody;
import id.advprog.c11.lsm.http.request.RegistrationRequestBody;
import id.advprog.c11.lsm.http.response.GenericResponse;
import id.advprog.c11.lsm.http.response.PembayaranResponseBody;
import id.advprog.c11.lsm.motor.Motor;
import id.advprog.c11.lsm.pembayaran.Pembayaran;
import id.advprog.c11.lsm.pembayaran.PembayaranFactory;
import id.advprog.c11.lsm.repository.ServisMotorRepository;
import id.advprog.c11.lsm.service.StatusService;
import id.advprog.c11.lsm.service.UserService;
import id.advprog.c11.lsm.servismotor.ServisMotor;
import id.advprog.c11.lsm.servismotor.ServisMotorFactory;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LsmController {

    private final ServisMotorRepository servisMotorRepository;
    private final UserService userService;
    private final StatusService statusService;

    LsmController(ServisMotorRepository servisMotorRepository, UserService userService,
                  StatusService statusService) {
        this.servisMotorRepository = servisMotorRepository;
        this.userService = userService;
        this.statusService = statusService;
    }

    @GetMapping("/")
    public String index() {
        return "Hello, world!";
    }

    @PostMapping("/pesan-servis")
    public ServisMotor pesanServis(@RequestBody Motor motor, Principal user) {
        ServisMotor servisMotor = ServisMotorFactory.createServis(motor,
                user.getName());
        servisMotorRepository.save(servisMotor);
        statusService.scheduleServis(servisMotor);
        return servisMotor;
    }

    @GetMapping("/riwayat")
    public List<ServisMotor> riwayat(Principal user) {
        return this.servisMotorRepository.findAllByUsername(user.getName());
    }

    @PostMapping("/register")
    public GenericResponse register(@RequestBody RegistrationRequestBody userRegistration) {
        this.userService.save(new User(
                userRegistration.getUsername(),
                userRegistration.getPassword(),
                Arrays.asList(new Role("USER")),
                true
        ));
        return new GenericResponse(true);
    }

    @GetMapping("/status")
    public ServisMotor status(@RequestParam("id") Long id, Principal user) {
        return this.servisMotorRepository.findByIdAndUsername(id, user.getName());
    }

    @PostMapping("/bayar")
    public PembayaranResponseBody bayar(
            @RequestBody PembayaranRequestBody detailPembayaran, Principal user
    ) {
        Pembayaran pembayaran = PembayaranFactory.createPembayaran(
                detailPembayaran.getTipe(), detailPembayaran.getNomorKartu()
        );
        ServisMotor servisMotor = servisMotorRepository.findByIdAndUsername(
                detailPembayaran.getIdServis(), user.getName()
        );
        pembayaran.setServisMotor(servisMotor);
        PembayaranResponseBody responseBody = new PembayaranResponseBody(pembayaran.bayar());
        servisMotorRepository.save(servisMotor);
        return responseBody;
    }

}
