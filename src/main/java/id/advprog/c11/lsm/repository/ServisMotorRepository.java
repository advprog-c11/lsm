package id.advprog.c11.lsm.repository;

import id.advprog.c11.lsm.servismotor.ServisMotor;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ServisMotorRepository extends JpaRepository<ServisMotor, Long> {
    ServisMotor findByIdAndUsername(long id, String username);

    List<ServisMotor> findAllByUsername(String username);
}
