package id.advprog.c11.lsm.repository;

import id.advprog.c11.lsm.authentication.entities.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
}
