package id.advprog.c11.lsm.pembayaran;

public class Tunai extends Pembayaran {
    private static final String tipe = "Tunai";

    public Tunai() {
        super(tipe);
    }

    public String bayar() {
        boolean isSudahDibayar = this.getServisMotor().isSudahDibayar();
        return super.bayar() + (isSudahDibayar ? "" : " dengan uang tunai.");
    }
}
