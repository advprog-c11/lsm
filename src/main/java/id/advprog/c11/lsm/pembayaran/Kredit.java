package id.advprog.c11.lsm.pembayaran;

public class Kredit extends Pembayaran {
    private static final String tipe = "Kredit";
    private static final String KETENTUAN = "Nomor kartu harus terdiri atas 16 digit angka.";
    private String nomorKartu;

    public Kredit(String nomorKartu) throws IllegalArgumentException {
        super(tipe);
        if (nomorKartu.length() != 16) {
            throw new IllegalArgumentException(KETENTUAN);
        }
        try {
            Double.parseDouble(nomorKartu);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(KETENTUAN);
        }
        this.nomorKartu = nomorKartu;
    }

    public String bayar() {
        boolean isSudahDibayar = this.getServisMotor().isSudahDibayar();
        return super.bayar() + (isSudahDibayar
             ? "" :
            String.format(
                " dengan kartu kredit yang berakhiran %s.",
                nomorKartu.substring(nomorKartu.length() - 4)
                )
            );
    }
}
