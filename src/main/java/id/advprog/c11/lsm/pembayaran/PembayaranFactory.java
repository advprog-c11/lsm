package id.advprog.c11.lsm.pembayaran;

public final class PembayaranFactory {
    private static final String TIPE_PEMBAYARAN_TIDAK_TERSEDIA = "Tipe pembayaran tidak tersedia!";

    public static Pembayaran createPembayaran(String tipe, String nomorKartu) {
        switch (tipe) {
            case "Debit":
                return new Debit(nomorKartu);
            case "Kredit":
                return new Kredit(nomorKartu);
            case "Tunai":
                return new Tunai();
            default:
                throw new IllegalArgumentException(TIPE_PEMBAYARAN_TIDAK_TERSEDIA);

        }
    }
}
