package id.advprog.c11.lsm.pembayaran;

import id.advprog.c11.lsm.servismotor.ServisMotor;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public abstract class Pembayaran {
    private String tipe;
    private ServisMotor servisMotor;

    public Pembayaran(String tipe) {
        this.tipe = tipe;
    }

    public String bayar() {
        if (this.servisMotor.isSudahDibayar()) {
            return "Servis ini sudah dibayar!";
        }
        this.servisMotor.setSudahDibayar(true);
        return String.format("Berhasil membayar sebesar Rp%.2f", this.servisMotor.getHarga());
    }
}
