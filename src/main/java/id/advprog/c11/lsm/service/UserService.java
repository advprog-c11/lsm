package id.advprog.c11.lsm.service;

import id.advprog.c11.lsm.authentication.entities.User;
import id.advprog.c11.lsm.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository repo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public User save(User user) {

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return repo.save(user);
    }

}
