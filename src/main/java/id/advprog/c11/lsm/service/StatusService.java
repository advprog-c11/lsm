package id.advprog.c11.lsm.service;

import id.advprog.c11.lsm.repository.ServisMotorRepository;
import id.advprog.c11.lsm.servismotor.ServisMotor;

import java.sql.Date;
import java.util.Calendar;
import java.util.concurrent.Executors;

import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Service;


@Service
public class StatusService {
    
    private ServisMotorRepository servisMotorRepository;
    private SimpMessagingTemplate simpMessagingTemplate;
    private static TaskScheduler scheduler =
            new ConcurrentTaskScheduler(Executors.newScheduledThreadPool(10));
    
    public StatusService(ServisMotorRepository servisMotorRepository,
                         SimpMessagingTemplate simpMessagingTemplate) {
        this.servisMotorRepository = servisMotorRepository;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    public void scheduleServis(ServisMotor servisMotor) {
        Runnable  selesaiTask = () -> updateServisStatus(servisMotor, 5, servisMotor.selesai());

        Runnable  cekBanTask = () -> {
            updateServisStatus(servisMotor, 4, servisMotor.cekBan());
            scheduler.schedule(
                    selesaiTask, new Date(Calendar.getInstance().getTimeInMillis()
                            +  servisMotor.getCekBanDelay())
            );
        };

        Runnable  cekMesinTask = () -> {
            updateServisStatus(servisMotor, 3, servisMotor.cekMesin());
            scheduler.schedule(
                     cekBanTask, new Date(Calendar.getInstance().getTimeInMillis()
                            +  servisMotor.getCekMesinDelay())
            );
        };

        Runnable  cekAkiTask = () -> {
            updateServisStatus(servisMotor, 2, servisMotor.cekAki());
            scheduler.schedule(
                     cekMesinTask, new Date(Calendar.getInstance().getTimeInMillis()
                            +  servisMotor.getCekAkiDelay())
            );
        };

        Runnable  cekRemTask = () -> {
            updateServisStatus(servisMotor, 1, servisMotor.cekRem());
            scheduler.schedule(
                     cekAkiTask, new Date(Calendar.getInstance().getTimeInMillis()
                            +  servisMotor.getCekRemDelay())
            );
        };

        scheduler.schedule(
                 cekRemTask, new Date(Calendar.getInstance().getTimeInMillis())
        );
    }

    public void updateServisStatus(ServisMotor servisMotor, int status, String info) {
        info = "Motor dengan plat nomor " + servisMotor.getMotor().getPlatNomor() + "\n" + info;
        servisMotor.setStatus(status);
        servisMotorRepository.save(servisMotor);
        simpMessagingTemplate.convertAndSend("/message/"
                + servisMotor.getUsername(), info);
    }
}
