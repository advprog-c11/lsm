package id.advprog.c11.lsm;

import id.advprog.c11.lsm.authentication.entities.Role;
import id.advprog.c11.lsm.authentication.entities.User;
import id.advprog.c11.lsm.service.UserService;
import java.util.Arrays;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class LsmApplication {
    @Bean
    PasswordEncoder getEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CommandLineRunner setupDefaultUser(UserService service) {
        return args -> {
            service.save(new User(
                    "user", //username
                    "user", //password
                    Arrays.asList(new Role("USER")),//roles
                    true//Active
            ));
        };
    }

    public static void main(String[] args) {
        SpringApplication.run(LsmApplication.class, args);
    }
}
