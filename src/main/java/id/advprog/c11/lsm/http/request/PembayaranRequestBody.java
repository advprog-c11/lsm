package id.advprog.c11.lsm.http.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PembayaranRequestBody {
    long idServis;
    String tipe;
    String nomorKartu;
}
