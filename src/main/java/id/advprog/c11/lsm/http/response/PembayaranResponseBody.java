package id.advprog.c11.lsm.http.response;

import lombok.Data;

@Data
public class PembayaranResponseBody {
    public boolean success;
    public String response;

    public PembayaranResponseBody(String response) {
        this.success = response.contains("Berhasil");
        this.response = response;
    }
}
