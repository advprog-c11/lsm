package id.advprog.c11.lsm.motor;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class Motor {
    private String tipe;
    private String platNomor;
    private String merek;
    private String warna;

    public String getDescription() {
        return String.format("Motor %s\nPlat Nomor: %s\nMerek: %s\n Warna: %s",
                this.getTipe(), this.getPlatNomor(), this.getMerek(), this.getWarna());
    }
}
