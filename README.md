# Layanan Servis Motor (LSM)

[![pipeline status](https://gitlab.com/advprog-c11/lsm/badges/master/pipeline.svg)](https://gitlab.com/advprog-c11/lsm/commits/master)
[![coverage report](https://gitlab.com/advprog-c11/lsm/badges/master/coverage.svg)](https://gitlab.com/advprog-c11/lsm/commits/master)

## [Live site](https://lsmotor.herokuapp.com)

##### Advanced Programming - C
##### Kelompok 11

| NPM        | Nama                       |
| ---------- | ----                       |
| 1706039383 | Muhammad Nadhirsyah Indra  |
| 1706979152 | Ahmad Fauzan Amirul Isnain |
| 1606917765 | Zunino Sultan Anggara      |
| 1706979455 | Sage Muhammad Abdullah     |
| 1406623865 | Irvanno Taqi Irmawan       |
